<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_owner = Role::where('name','Owner')->first();
        $role_manager = Role::where('name','Manager')->first();
        $role_sales = Role::where('name','Sales')->first();

        $user = new User();
        $user->name = 'Owner';
        $user->email = 'owner@gmail.com';
        $user->phone = '123123123';
        $user->password = bcrypt('siteowner');
        $user->avatar = 'avatar-placeholder.png';
        $user->save();
        $user->roles()->attach($role_owner);

        $user = new User();
        $user->name = 'Manager';
        $user->email = 'manager@gmail.com';
        $user->phone = '123123123';
        $user->password = bcrypt('sitemanager');
        $user->avatar = 'avatar-placeholder.png';
        $user->save();
        $user->roles()->attach($role_manager);

        $user = new User();
        $user->name = 'Sales';
        $user->email = 'sales@gmail.com';
        $user->phone = '123123123';
        $user->password = bcrypt('sitesales');
        $user->avatar = 'avatar-placeholder.png';
        $user->save();
        $user->roles()->attach($role_sales);
    }
}
