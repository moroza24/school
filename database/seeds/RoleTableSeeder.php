<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'Owner';
        $role->description = 'Site Admin';
        $role->save();

        $role = new Role();
        $role->name = 'Manager';
        $role->description = 'Manager User';
        $role->save();

        $role = new Role();
        $role->name = 'Sales';
        $role->description = 'Sales User';
        $role->save();
    }
}
