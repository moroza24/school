@extends('layouts.app')

@section('page-content') 
<div class="container-fluid">
    <div class="home-dashboard row">
        <div class="col-xs-12 col-sm-3 clear-col">
            @include('courses.index')
        </div>
        <div class="col-xs-12 col-sm-3 clear-col">
            @include('students.index')
        </div>
        <div class="col-xs-12 col-sm-6 clear-col">
            @yield('main-content')
        </div>
    </div>
</div>
    
@endsection

