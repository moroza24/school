@extends('layouts.app')

@section('page-content')
<div class="container-fluid">
    <div class="home-dashboard row">
        <div class="col-xs-12 col-sm-3 clear-col">
            @include('administration.index')
        </div>
        <div class="col-xs-12 col-sm-9 clear-col">
            @yield('main-content')
        </div>
    </div>
@endsection