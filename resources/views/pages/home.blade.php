@extends('layouts.index')

@section('main-content')
    <div class="main-container main-clean">
        @include('inc.school-statistics')
    </div>
@endsection