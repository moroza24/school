<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>401 - Not Authorized</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="page-error">
        <h1>401</h1>
        <h2>Not Authorized</h2>
        <p>Well, what to say. Hmm? Sorry ...</p>
        <p><a href="/">home</a></p>
    </div>
</body>
</html>

