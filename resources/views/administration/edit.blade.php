@extends('layouts.dashboard')

@section('main-content')
    <div class="main-container main2">

        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Update Admin</h1>
                    <br>
                </div>

                <div class="col-xs-12">
                    {!! Form::open(['action' => ['AdminController@update', $admin->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                    
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('name','Name')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{Form::text('name', $admin->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('email','Email')}}
                        </div>
                        
                        <div class="col-xs-12 col-sm-6">
                            {{Form::email('email', $admin->email, ['class' => 'form-control', 'placeholder' => 'Email'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('phone','Phone')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{Form::text('phone', $admin->phone, ['class' => 'form-control', 'placeholder' => 'Phone'])}}
                        </div>
                    </div>

                    <!-- Authentication Links -->
                    @if(Auth::user()->hasAnyRole(['Owner','Manager']))
                        @if(Auth::user()->id == $admin->id && Auth::user()->hasAnyRole(['Owner','Manager']))
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-2">
                                    {{Form::label('role','Role')}}
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    {{ Form::select('role', [
                                        'Manager' => 'Manager',
                                        'Sales' => 'Sales'],
                                        $admin->roles[0]->name,
                                        ['class' => 'form-control', 'placeholder' => 'Select Role...', 'disabled']
                                        ) }}
                                </div>
                            </div>
                        @else
                            <div class="form-group row">
                                <div class="col-xs-12 col-sm-2">
                                    {{Form::label('role','Role')}}
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    {{ Form::select('role', [
                                        'Manager' => 'Manager',
                                        'Sales' => 'Sales'],
                                        $admin->roles[0]->name,
                                        ['class' => 'form-control', 'placeholder' => 'Select Role...']
                                        ) }}
                                </div>
                            </div>
                        @endif
                    @endif
                    <div class="form-group">
                        {{Form::file('avatar')}}
                    </div>
                    {{Form::submit('Update', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}

                    <br>
                    
                    <!-- Authentication Links -->
                    @if(Auth::user()->id != $admin->id && Auth::user()->hasAnyRole(['Manager','Owner']))
                        {!!Form::open(['action' => ['AdminController@destroy', $admin->id], 'method' => 'POST', 'class' => 'pull-left'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                        {!!Form::close()!!}
                    @endif
                </div>
            </div>
        </div>
        
        <br>
        

        
    </div>
@endsection


