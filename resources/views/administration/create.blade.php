@extends('layouts.dashboard')

@section('main-content')
    <div class="main-container main2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Add Admin</h1>
                    <br>
                </div>

                <div class="col-xs-12">
                    {!! Form::open(['action' => 'AdminController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('name','Name')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('email','Email')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('phone','Phone')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'Phone'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('role','Role')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{ Form::select('role', [
                                'manager' => 'Manager',
                                'sales' => 'Sales'], null,
                                ['class' => 'form-control', 'placeholder' => 'Select Role...']
                                ) }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('password','Password')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('password_confirmation','Confirm Password')}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            {{Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12">
                            {{Form::file('avatar')}}
                        </div>
                    </div>
                    {{Form::submit('Create', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        

        
    </div>
@endsection