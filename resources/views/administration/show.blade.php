@extends('layouts.dashboard')


@section('main-content')
    <div class="main-container main2">
        <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>{{$admin->name}} <a href="/dashboard/{{$admin->id}}/edit"><i class="fas fa-xs fa-cog"></i></a></h1>
                        <br><br>
                    </div>        
                
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12 col-md-2">
                                        <p>Email: &nbsp;</p>
                                    </div>
                                    <div class="col-xs-12 col-md-10">
                                        <p>{{$admin->email}}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-md-2">
                                        <p>Phone: &nbsp;</p>
                                    </div>
                                    <div class="col-xs-12 col-md-10">
                                        <p>{{$admin->phone}}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-md-2">
                                        <p>Role: </p>
                                    </div>
                                    <div class="col-xs-12 col-md-10">
                                        <p>{{$admin->roles[0]->name}}</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-6" style="align-content: center;">
                                <div class="imageContainer imageContainer-sm">
                                    <div class="imageHolder">
                                        <img class="imageItself" src="/storage/avatars/{{$admin->avatar}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>
               
        </div>
    </div>
@endsection