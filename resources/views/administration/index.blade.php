<div class="list">
        <div class="header">
            <h3>Administrators</h3>
            <a href='/dashboard/create' class="add"><i class="fas fa-plus"></i></a>
        </div>
        <div class="body">
            @if(count($administrators) > 0)
                @foreach($administrators as $admin)
                    @if($admin->hasRole('Owner') && Auth::user()->hasRole('Manager'))
                    @else
                        <div class="list-item">
                                <div class="imageContainer imageContainer-xs">
                                    <div class="imageHolder">
                                        <img class="imageItself"  src="/storage/avatars/{{$admin->avatar}}">
                                    </div>
                                </div>
                            <div class="content">
                                <h4><a href="/dashboard/{{$admin->id}}">{{$admin->name}}</a></h4>
                                <p>{{$admin->email}}</p>
                            </div>
                        </div>
                    @endif
                @endforeach
            
            @else
                <p>No Courses Found</p>
            @endif
    
        </div>
    </div>