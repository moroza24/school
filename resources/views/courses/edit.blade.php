@extends('layouts.index')

@section('main-content')
    <div class="main-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Update Course</h1>
                    <br>
                </div>

                <div class="col-xs-12">
                    {!! Form::open(['action' => ['CoursesController@update', $course->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('name','Name')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::text('name', $course->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('description','Description')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::textarea('description', $course->description, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Description'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12">
                            {{Form::file('course_image')}}
                        </div>
                    </div>
                    {{Form::submit('Save', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                        <br>
                        
                    {{--  TODO: add a nice confirmation message  --}}
                    {!!Form::open(['action' => ['CoursesController@destroy', $course->id], 'method' => 'POST', 'class' => 'pull-left', 'onsubmit' => 'return confirm("Are you sure?")'])!!}
                            {{Form::hidden('_method', 'DELETE')}}
                            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                    {!!Form::close()!!}
                </div>
            </div>
        </div>

        
        
    </div>
@endsection