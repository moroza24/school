@extends('layouts.index')

@section('main-content')
    <div class="main-container">
        <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>{{$course->name}} 
                            @if(Auth::user())
                                @if(!Auth::user()->hasRole('Sales'))
                                    <a href="/courses/{{$course->id}}/edit"><i class="fas fa-xs fa-cog"></i></a>
                                @endif
                            @endif
                        </h1>
                        <small>Enrolled: {{count($course->students)}}</small>
                        <br><br>
                    </div>        
                
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p>{{$course->description}}</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xs-12 col-sm-6" style="align-content: center;">
                                <div class="imageContainer imageContainer-sm">
                                    <div class="imageHolder">
                                        <img class="imageItself" src="/storage/course_image/{{$course->course_image}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>

                
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Enrolled Courses</h3>
                    </div>
                    <div class="col-xs-12">
                        @if(count($course->students) > 0)
                            <div class="enrolled-courses-list">
                                @foreach($course->students as $student)
                                    <div class="enrolled-courses-item">
                                        <div class="imageContainer imageContainer-xs">
                                            <div class="imageHolder">
                                                <img class="imageItself" src="/storage/avatars/{{$student->avatar}}" />
                                            </div>
                                        </div>
                                        <p><a href="/students/{{$student->id}}">{{$student->name}}</a></p>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <p>This Course have no students enrolled</p>
                        @endif
                    </div>
                </div>
                
                
        </div>
    </div>
@endsection