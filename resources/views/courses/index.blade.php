<div class="list">
        <div class="header">
            <h3>Courses</h3>
            
            <!-- Authentication Links -->
            @if(Auth::user())
                @if(!Auth::user()->hasRole('Sales'))
                    <a href='/courses/create' class="add"><i class="fas fa-plus"></i></a>
                @endif
            @endif
        </div>
        <div class="body">
            @if(count($courses) > 0)
                @foreach($courses as $course)
                <div class="list-item">
                        <div class="imageContainer imageContainer-xs">
                            <div class="imageHolder">
                                <img class="imageItself"  src="/storage/course_image/{{$course->course_image}}">
                            </div>
                        </div>
                            
                        <div class="content">
                            <h4><a href="/courses/{{$course->id}}">{{$course->name}}</a></h4>
                            <p>{{$course->description}}</p>
                        </div>
                    </div>
                @endforeach
            
            @else
                <p>No Courses Found</p>
            @endif
    
        </div>
    </div>