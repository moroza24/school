@extends('layouts.index')

@section('main-content')
    <div class="main-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Create Course</h1>
                    <br>
                </div>
                
                <div class="col-xs-12">
                    {!! Form::open(['action' => 'CoursesController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('name','Name')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('description','Description')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::textarea('description', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Description'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12">
                            {{Form::file('course_image')}}
                        </div>
                    </div>
                    {{Form::submit('Create', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection