<div class="dash-panel">
        <div class="card">
            <div class="card-header">
                <span><i class="fas fa-gem fa-4x"></i></span>
                <span>
                    <h3>Admins</h3>
                    <p>{{count($administrators)}}</p>
                </span>
            </div>
            {{--  <div class="card-footer">
                <a href="/dashboard">
                    <div class="card-details">
                        <span class="pull-left">View Details </span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    </div>
                </a>
            </div>  --}}
        </div>
    </div>