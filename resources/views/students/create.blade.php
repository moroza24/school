@extends('layouts.index')

@section('main-content')
    <div class="main-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Add Student</h1>
                    <br>
                </div>

                <div class="col-xs-12">
                    {!! Form::open(['action' => 'StudentsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('name','Name')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('phone','Phone')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'Phone'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('email','Email')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12">
                            {{Form::file('avatar')}}
                        </div>
                    </div>

                    @if(count($courses) > 0)
                        <div class="enrolled-courses-list">
                            @foreach($courses as $course)
                                <div class="enrolled-courses-item">
                                    <div class="imageContainer imageContainer-xs">
                                        <div class="imageHolder">
                                            <img class="imageItself" src="/storage/course_image/{{$course->course_image}}" />
                                        </div>
                                    </div>
                                    {{Form::checkbox( 'enrolled[]', $course->id)}}
                                    {{Form::label( 'enrolled', $course->name)}}
                                </div>
                            @endforeach
                        </div>
                    @endif

                    {{Form::submit('Create', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection