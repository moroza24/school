@extends('layouts.index')

@section('main-content')
    <div class="main-container">
        <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>{{$student->name}} <a href="/students/{{$student->id}}/edit"><i class="fas fa-xs fa-cog"></i></a></h1>
                        <small>Enrolled: {{count($student->courses)}}</small>
                        <br><br>
                    </div>        
                
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3>Student Detailes</h3>
                                    </div>
                
                                    <div class="col-xs-12 col-md-2">
                                        <p>Phone: </p>
                                    </div>
                                    <div class="col-xs-12 col-md-10">
                                        <p>{{$student->phone}}</p>
                                    </div>
                
                                    <div class="col-xs-12 col-md-2">
                                        <p>Email: </p>
                                    </div>
                                    <div class="col-xs-12 col-md-10">
                                        <p>{{$student->email}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6" style="align-content: center;">
                                <div class="imageContainer imageContainer-sm">
                                    <div class="imageHolder">
                                        <img class="imageItself" src="/storage/avatars/{{$student->avatar}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>

                
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Enrolled Courses</h3>
                    </div>
                    <div class="col-xs-12">
                        @if(count($student->courses) > 0)
                            <div class="enrolled-courses-list">
                                @foreach($student->courses as $course)
                                    <div class="enrolled-courses-item">
                                        <div class="imageContainer imageContainer-xs">
                                            <div class="imageHolder">
                                                <img class="imageItself" src="/storage/course_image/{{$course->course_image}}" />
                                            </div>
                                        </div>
                                        <p><a href="/courses/{{$course->id}}">{{$course->name}}</a></p>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <p>This student is not enrolled to any course yet</p>
                        @endif
                    </div>
                </div>
                
                
        </div>
    </div>
@endsection