@extends('layouts.index')

@section('main-content')
    <div class="main-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Update Student</h1>
                    <br>
                </div>

                <div class="col-xs-12">
                    {!! Form::open(['action' => ['StudentsController@update', $student->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                    
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('name','Name')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::text('name', $student->name, ['class' => 'form-control', 'placeholder' => 'Name'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">
                            {{Form::label('phone','Phone')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::text('phone', $student->phone, ['class' => 'form-control', 'placeholder' => 'Phone'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-2">                        
                            {{Form::label('email','Email')}}
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            {{Form::email('email', $student->email, ['class' => 'form-control', 'placeholder' => 'Email'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-12">
                            {{Form::file('avatar')}}
                        </div>
                    </div>

                    @if(count($courses) > 0)
                        <h3>Enrolled Courses</h3>
                        <div class="enrolled-courses-list">
                            @foreach($courses as $course)


                                <div class="form-group course-check">
                                    <div class="form-group course-check">

                                        <div class="enrolled-courses-item">
                                            <div class="imageContainer imageContainer-xs">
                                                <div class="imageHolder">
                                                    <img class="imageItself" src="/storage/course_image/{{$course->course_image}}" />
                                                </div>
                                            </div>
                                            @if(in_array($course->id, $enrolledCoursesIds))
                                                {{Form::checkbox( 'enrolled[]', $course->id, true)}}
                                            @else
                                                {{Form::checkbox( 'enrolled[]', $course->id)}}
                                            @endif
                                            {{Form::label( 'enrolled', $course->name)}}
                                        </div>
                                    </div>
                                </div>


                            @endforeach
                        </div>
                    @endif
                    {{--  TODO: add a nice confirmation message  --}}
                    {{Form::submit('Save', ['class' => 'btn btn-primary'])}}
                    {!! Form::close() !!}
                        <br>
                        
                    <!-- Authentication Links -->
                    @if(Auth::user())
                        @if(!Auth::user()->hasRole('Sales')) 
                            {!!Form::open(['action' => ['StudentsController@destroy', $student->id], 'method' => 'POST', 'class' => 'pull-left', 'onsubmit' => 'return confirm("Are you sure?")'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                            {!!Form::close()!!}
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection