<div class="list">
        <div class="header">
            <h3>Students</h3>
            <!-- Authentication Links -->
            @if(Auth::user())
                @if(!Auth::user()->hasRole('Sales')) 
                    <a href='/students/create' class="add"><i class="fas fa-plus"></i></a>
                @endif
            @endif
            
        </div>
        <div class="body">
            @if(count($students) > 0) 
                @foreach($students as $student)
                    <div class="list-item">
                            <div class="imageContainer imageContainer-xs">
                                <div class="imageHolder">
                                    <img class="imageItself" src="/storage/avatars/{{$student->avatar}}">
                                </div>
                            </div>
                        <div class="content">
                            <h4><a href="/students/{{$student->id}}">{{$student->name}}</a></h4>
                            <p>{{$student->phone}}</p>
                        </div>
                    </div>
                @endforeach
            @else
                <p>No Students Found</p>
            @endif
        </div>
    </div>