## Users 
Owner
Username: owner@gmail.com
Password: siteowner

Manager
Username: manager@gmail.com
Password: sitemanager

Sales
Username: sales@gmail.com
Password: sitesales


## Controllers & Modals
AdminController
CoursesController
StudentsController
PagesController

CoursesControllerRole
StudentsControllerUser


## Middleware 
CheckRole
Auth


## Database Tables 
users
courses
courses_students
roles
role_user


## Seeder
RoleTableSeeder
UsersTableSeeder


## Style 
Bootstrap
custom.scss
errors.scss
components/card.scss
components/imageHolder.scss
components/list.scss


## Views 
administration/index
administration/show
administration/edit
administration/create

courses/index
courses/show
courses/edit
courses/create

students/index
students/show
students/edit
students/create

pages/home
pages/administration
pages/notAuthorized

inc/messages
inc/navbar
inc/school-statistics
inc/admin-statistics

layouts/app
layouts/dashboard
layouts/index


## Routes 

+--------+-----------+----------------------------+-------------------+------------------------------------------------------------------------+----------------+
| Domain | Method    | URI                        | Name              | Action                                                                 | Middleware     |
+--------+-----------+----------------------------+-------------------+------------------------------------------------------------------------+----------------+
|        | GET|HEAD  | /                          |                   | App\Http\Controllers\PagesController@index                             | web,auth       |
|        | GET|HEAD  | api/user                   |                   | Closure                                                                | api,auth:api   |
|        | POST      | courses                    | courses.store     | App\Http\Controllers\CoursesController@store                           | web            |
|        | GET|HEAD  | courses                    | courses.index     | App\Http\Controllers\CoursesController@index                           | web            |
|        | GET|HEAD  | courses/create             | courses.create    | App\Http\Controllers\CoursesController@create                          | web,roles      |
|        | DELETE    | courses/{course}           | courses.destroy   | App\Http\Controllers\CoursesController@destroy                         | web,roles      |
|        | PUT|PATCH | courses/{course}           | courses.update    | App\Http\Controllers\CoursesController@update                          | web            |
|        | GET|HEAD  | courses/{course}           | courses.show      | App\Http\Controllers\CoursesController@show                            | web            |
|        | GET|HEAD  | courses/{course}/edit      | courses.edit      | App\Http\Controllers\CoursesController@edit                            | web,roles      |
|        | GET|HEAD  | dashboard                  | dashboard.index   | App\Http\Controllers\AdminController@index                             | web,auth,roles |
|        | POST      | dashboard                  | dashboard.store   | App\Http\Controllers\AdminController@store                             | web,auth,roles |
|        | GET|HEAD  | dashboard/create           | dashboard.create  | App\Http\Controllers\AdminController@create                            | web,auth,roles |
|        | GET|HEAD  | dashboard/{dashboard}      | dashboard.show    | App\Http\Controllers\AdminController@show                              | web,auth,roles |
|        | DELETE    | dashboard/{dashboard}      | dashboard.destroy | App\Http\Controllers\AdminController@destroy                           | web,auth,roles |
|        | PUT|PATCH | dashboard/{dashboard}      | dashboard.update  | App\Http\Controllers\AdminController@update                            | web,auth,roles |
|        | GET|HEAD  | dashboard/{dashboard}/edit | dashboard.edit    | App\Http\Controllers\AdminController@edit                              | web,auth,roles |
|        | GET|HEAD  | login                      | login             | App\Http\Controllers\Auth\LoginController@showLoginForm                | web,guest      |
|        | POST      | login                      |                   | App\Http\Controllers\Auth\LoginController@login                        | web,guest      |
|        | POST      | logout                     | logout            | App\Http\Controllers\Auth\LoginController@logout                       | web            |
|        | GET|HEAD  | notAuthorized              |                   | App\Http\Controllers\PagesController@notAuthorized                     | web,auth       |
|        | POST      | password/email             | password.email    | App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail  | web,guest      |
|        | POST      | password/reset             |                   | App\Http\Controllers\Auth\ResetPasswordController@reset                | web,guest      |
|        | GET|HEAD  | password/reset             | password.request  | App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm | web,guest      |
|        | GET|HEAD  | password/reset/{token}     | password.reset    | App\Http\Controllers\Auth\ResetPasswordController@showResetForm        | web,guest      |
|        | GET|HEAD  | register                   | register          | App\Http\Controllers\Auth\RegisterController@showRegistrationForm      | web            |
|        | POST      | register                   |                   | App\Http\Controllers\Auth\RegisterController@register                  | web            |
|        | GET|HEAD  | students                   | students.index    | App\Http\Controllers\StudentsController@index                          | web            |
|        | POST      | students                   | students.store    | App\Http\Controllers\StudentsController@store                          | web            |
|        | GET|HEAD  | students/create            | students.create   | App\Http\Controllers\StudentsController@create                         | web,roles      |
|        | GET|HEAD  | students/{student}         | students.show     | App\Http\Controllers\StudentsController@show                           | web            |
|        | PUT|PATCH | students/{student}         | students.update   | App\Http\Controllers\StudentsController@update                         | web            |
|        | DELETE    | students/{student}         | students.destroy  | App\Http\Controllers\StudentsController@destroy                        | web,roles      |
|        | GET|HEAD  | students/{student}/edit    | students.edit     | App\Http\Controllers\StudentsController@edit                           | web            |
+--------+-----------+----------------------------+-------------------+------------------------------------------------------------------------+----------------+