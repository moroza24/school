<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    // Table name
    protected $table = 'students';
    // Primary key
    public $primaryKey = 'id';
    // Timestamp
    public $timestamp = true;

    public function courses()
    {
        return $this->belongsToMany('App\Courses');
    }
}
