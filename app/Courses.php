<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class courses extends Model
{
    // Table name
    protected $table = 'courses';
    // Primary key
    public $primaryKey = 'id';
    // Timestamp
    public $timestamp = true;

    public function students()
    {
        return $this->belongsToMany('App\Students');
    }
}
