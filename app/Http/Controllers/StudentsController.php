<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Courses;
use App\Students;
use DB;

class StudentsController extends Controller
{
    

    public function __construct()
    {
        // authenticat user access 
        $this->middleware('roles', ['only' => ['create','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'avatar' => 'image|nullable|max:1999'
        ]);

        // Handel file upload
        if($request->hasFile('avatar')){
            // get file name with ext
            $filenameWithExt = $request->file('avatar')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just the ext
            $extansion = $request->file('avatar')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename .'_' .time() .'.' .$extansion;
            // Upload the Image
            $path = $request->file('avatar')->storeAs('public/avatars', $fileNameToStore);
        } else {
            $fileNameToStore = 'avatar-placeholder.png';
        }
        
        // Create Student
        $student = new Students;
        $student->name = $request->input('name');
        $student->email = $request->input('email');
        $student->phone = $request->input('phone');
        $student->avatar = $fileNameToStore;
        $student->save();

        $student->courses()->sync($request->input('enrolled'), true);

        return redirect('/')->with('success', 'Student Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Students::find($id);
        return view('students.show')->with('student', $student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Students::find($id);

        $enrolledCourses = $student->courses;
        $enrolledCoursesIds = array();

        foreach($enrolledCourses as $enrolledCourse){
            $enrolledCoursesIds[] = $enrolledCourse['id'];
        }


        $data = [
            'student' => $student,
            'enrolledCoursesIds' => $enrolledCoursesIds
        ];
        
        return view('students.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'avatar' => 'image|nullable|max:1999'
        ]);

        // Handel file upload
        if($request->hasFile('avatar')){
            // get file name with ext
            $filenameWithExt = $request->file('avatar')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just the ext
            $extansion = $request->file('avatar')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename .'_' .time() .'.' .$extansion;
            // Upload the Image
            $path = $request->file('avatar')->storeAs('public/avatars', $fileNameToStore);
        } else {
            $fileNameToStore = 'avatar-placeholder.png';
        }
        
        // Create Student
        $student = Students::find($id);
        $student->name = $request->input('name');
        $student->email = $request->input('email');
        $student->phone = $request->input('phone');
        if($request->hasFile('avatar')){
            if($student->avatar != 'avatar-placeholder.png'){
                // Delete image
                Storage::delete('public/avatars/' .$student->avatar);
            }
            $student->avatar = $fileNameToStore;
        }
        $student->save();
        
        $student->courses()->sync($request->input('enrolled'), true);
        
        return redirect('/')->with('success', 'Student Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Students::find($id);

        if($student->avatar != 'avatar-placeholder.png'){
            // Delete image
            Storage::delete('public/avatars/' .$student->avatar);
        }
        $student->courses()->detach();
        $student->delete();
        return redirect('/')->with('success', 'Student Deleted');
    }
}
