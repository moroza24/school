<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Courses;
use App\Students;
use DB;

class CoursesController extends Controller
{
    public function __construct()
    {
        // authenticat user access 
        $this->middleware('roles', ['only' => ['edit','create','destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'course_image' => 'image|nullable|max:1999'
        ]);

        // Handel file upload
        if($request->hasFile('course_image')){
            // get file name with ext
            $filenameWithExt = $request->file('course_image')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just the ext
            $extansion = $request->file('course_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename .'_' .time() .'.' .$extansion;
            // Upload the Image
            $path = $request->file('course_image')->storeAs('public/course_image', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
        
        // Create Course
        $course = new Courses;
        $course->name = $request->input('name');
        $course->description = $request->input('description');
        $course->course_image = $fileNameToStore;
        $course->save();

        return redirect('/')->with('success', 'Course Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Courses::find($id);
        
        return view('courses.show')->with('course', $course);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Courses::find($id);
        
        $data = [
            'course' => $course
        ];
        
        return view('courses.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'course_image' => 'image|nullable|max:1999'
        ]);

        // Handel file upload
        if($request->hasFile('course_image')){
            // get file name with ext
            $filenameWithExt = $request->file('course_image')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just the ext
            $extansion = $request->file('course_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename .'_' .time() .'.' .$extansion;
            // Upload the Image
            $path = $request->file('course_image')->storeAs('public/course_image', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }
        
        // fine Course

        $course = Courses::find($id);
        
        $course->name = $request->input('name');
        $course->description = $request->input('description');
        if($request->hasFile('course_image')){
            if($course->course_image != 'noimage.jpg'){
                // Delete image
                Storage::delete('public/course_image/' .$course->course_image);
            }
            $course->course_image = $fileNameToStore;
        }
    
        $course->save();

        return redirect('/')->with('success', 'Course Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Courses::find($id);

        if($course->course_image != 'noimage.jpg'){
            // Delete image
            Storage::delete('public/course_image/' .$course->course_image);
        }
        $course->students()->detach();
        $course->delete();
        return redirect('/')->with('success', 'Course Deleted');
    }
    
}
