<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Courses;
use App\Students;

class PagesController extends Controller
{
    public function __construct()
    {
        // authenticat user access 
        $this->middleware('auth');
    }

    public function index(){
        $title = 'Welcome to The School';
        
        $data = [
            'title' => $title
        ];
        return view('pages.home')->with($data);
    }

    public function notAuthorized(){
        
        return view('pages.notAuthorized');
    }
}
