<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Courses;
use App\Students;
use App\User;
use App\Role;
use DB;

class AdminController extends Controller
{

    public function __construct()
    {
        // authenticat user access 
        $this->middleware('auth');
        $this->middleware('roles');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('pages.administration')->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administration.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'string',
            'role' => 'required|string',
            'avatar' => 'file',
        ]);

        // Handel file upload
        if($request->hasFile('avatar')){
            // get file name with ext
            $filenameWithExt = $request->file('avatar')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just the ext
            $extansion = $request->file('avatar')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename .'_' .time() .'.' .$extansion;
            // Upload the Image
            $path = $request->file('avatar')->storeAs('public/avatars', $fileNameToStore);
        } else {
            $fileNameToStore = 'avatar-placeholder.png';
        }
        
        // Create Student
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $role = Role::where('name', $request->input('role'))->first();
        $user->password = bcrypt($request->input['password']);
        $user->avatar = $fileNameToStore;
        $user->save();

        $user->roles()->detach();
        $user->roles()->attach($role);

        
        return redirect('/dashboard')->with('success', 'Admin Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = User::find($id);
        
        return view('administration.show')->with('admin', $admin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $admin = User::find($id);
        
        $data = [
            'admin' => $admin
        ];
        
        return view('administration.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'string',
            'role' => 'required|string',
            'avatar' => 'file',
        ]);

        // Handel file upload
        if($request->hasFile('avatar')){
            // get file name with ext
            $filenameWithExt = $request->file('avatar')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just the ext
            $extansion = $request->file('avatar')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename .'_' .time() .'.' .$extansion;
            // Upload the Image
            $path = $request->file('avatar')->storeAs('public/avatars', $fileNameToStore);
        } else {
            $fileNameToStore = 'avatar-placeholder.png';
        }
        
        // Update User
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $role = Role::where('name', $request->input('role'))->first();
        if($request->hasFile('avatar')){
            if($user->avatar != 'avatar-placeholder.png'){
                // Delete image
                Storage::delete('public/avatars/' .$user->avatar);
            }
            $user->avatar = $fileNameToStore;
        }
        $user->save();
        $user->roles()->detach();
        $user->roles()->attach($role);

        
        return redirect('/dashboard')->with('success', 'Admin Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if($user->avatar != 'avatar-placeholder.png'){
            // Delete image
            Storage::delete('public/avatars/' .$user->avatar);
        }
        
        $user->roles()->detach();
        $user->delete();
        return redirect('/dashboard')->with('success', 'Admin Deleted');
    }
}
